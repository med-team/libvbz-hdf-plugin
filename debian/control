Source: libvbz-hdf-plugin
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>,
           Étienne Mollier <emollier@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               cmake,
               libzstd-dev,
               libhdf5-dev,
               libstreamvbyte-dev,
               dh-exec
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/med-team/libvbz-hdf-plugin
Vcs-Git: https://salsa.debian.org/med-team/libvbz-hdf-plugin.git
Homepage: https://github.com/nanoporetech/vbz_compression/
Rules-Requires-Root: no

Package: libvbz-hdf-plugin0
Architecture: any
Section: libs
Depends: ${shlibs:Depends},
         ${misc:Depends}
Multi-Arch: same
Description: VBZ compression plugin for nanopore signal data
 VBZ Compression uses variable byte integer encoding to compress nanopore
 signal data.
 .
 The performance of VBZ is achieved by taking advantage of the properties
 of the raw signal and therefore is most effective when applied to the
 signal dataset. Other datasets you may have in your Fast5 files will not
 be able to take advantage of the default VBZ settings for compression.
 VBZ will be used as the default compression scheme in a future release
 of MinKNOW.
 .
 This package contains the shared object library.

Package: libvbz-hdf-plugin-dev
Architecture: any
Section: libdevel
Depends: libvbz-hdf-plugin0 (= ${binary:Version}),
         ${shlibs:Depends},
         ${misc:Depends}
Multi-Arch: same
Description: VBZ compression plugin for nanopore signal data (devel)
 VBZ Compression uses variable byte integer encoding to compress nanopore
 signal data.
 .
 The performance of VBZ is achieved by taking advantage of the properties
 of the raw signal and therefore is most effective when applied to the
 signal dataset. Other datasets you may have in your Fast5 files will not
 be able to take advantage of the default VBZ settings for compression.
 VBZ will be used as the default compression scheme in a future release
 of MinKNOW.
 .
 This package contains the header files.
